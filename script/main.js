//menu
const leftMenu = document.querySelector('.left-menu');
const hamburger = document.querySelector('.hamburger');

//открытие-закрытие меню
hamburger.addEventListener('click', () => {
    leftMenu.classList.toggle('openMenu');
    hamburger.classList.toggle('open');
});

//закрытие меню при клике на пустоту
document.addEventListener('click', (event) => {
    if (!event.target.closest('.left-menu')) {
        leftMenu.classList.remove('openMenu');
        hamburger.classList.remove('open');
    }
})
//дропдауны
leftMenu.addEventListener('click', event => {
    const target = event.target;
    const dropdown = target.closest('.dropdown');
    if (dropdown) {
        dropdown.classList.toggle('active');
        leftMenu.classList.add('openMenu');
        hamburger.classList.add('open');
    }
});
//ховер картинки ДЗ
function JShover() {
    document.querySelectorAll('[src]').forEach(img => {
        const src = img.getAttribute('src');
        const dataDrop = img.getAttribute('data-backdrop');
        img.addEventListener('mouseover', () => {img.src = dataDrop;})
        img.addEventListener('mouseout', () => {img.src = src;})
    });
}
JShover();